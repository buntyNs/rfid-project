import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import firebase from './firebase';
import axios from 'axios';
import Details from './componets/Details';
import Register from './componets/Register';
import Home from './componets/Home';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: "",
      student: {},
      length: 1,
      home: true,

    }
  }

  componentWillMount() {

    const code = firebase.database().ref('students');

    code.on('value', snapshot => {
    });
    code.on('value', (value) => {
      let newCode = value.val();
      if (newCode.code === "") {

      } else {

        axios.get('http://139.59.58.237:3000/user' + newCode.code)
          .then(todos => {
            this.setState({
              student: todos.data.data[0],
              length: todos.data.data.length
            });
          }).then(function () {
            code.set({
              code: ""
            });
          });

        this.setState({
          code: newCode,
          home: false
        })
      }
    })
  }


  click = () => {
    this.setState({
      home: true,
      student: [],
      length: 0,
    });
  }

  render() {

    var test = this.state.student;

    let view;
    if (this.state.home) {
      view = (<Home></Home>);
    } else {
      if (this.state.length == 1) {
        view = (
          <Details data={this.state.student} click={this.click.bind(this)} />
        );

      } else {
        view = (<Register code = {this.state.code}  close = {this.click.bind(this)}/>);
      }

    }

    return (
      <div className="App">
        {view}
      </div>
    );
  }
}

export default App;
