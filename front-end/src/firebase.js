import firebase from 'firebase';
// Initialize Firebase
const config = {
    apiKey: "AIzaSyA0Fq3rgWdbBFQN86vhuo0TPGEmR2DHXHA",
    authDomain: "rfid-e7c41.firebaseapp.com",
    databaseURL: "https://rfid-e7c41.firebaseio.com",
    projectId: "rfid-e7c41",
    storageBucket: "rfid-e7c41.appspot.com",
    messagingSenderId: "816856557167"
  };
  firebase.initializeApp(config);

  export default firebase;