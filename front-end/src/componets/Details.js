import React from 'react';
import user from '../user.png';
import { Button } from 'reactstrap';
import ReactAudioPlayer from 'react-audio-player';
import beep from '../intuition.mp3'
const Details = (props)=>{
    return(
        
       <div className = "card">
          {/* <ReactAudioPlayer
                src = {beep}
                autoPlay
            /> */}
       <img className = "img-style" src={user} alt="Avatar"/>
       <h1>{props.data.name}</h1>
       <div style = {{textAlign:"left",paddingLeft:'15px'}}>
        <p><span>Age : </span>{props.data.age}</p>
        <p><span>Email : </span>{props.data.email}</p>
        <p><span>id : </span>{props.data.code}</p>
        <Button style = {{float:'right'}} onClick = {props.click}>Okay</Button>
       </div>
       
       </div>
    
    );
}
export default Details;


