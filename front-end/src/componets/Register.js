import React, { Component } from 'react';
import user from '../user.svg';
import beep from '../beep.mp3'
import ReactAudioPlayer from 'react-audio-player';
import axios from 'axios';
import {Form, FormGroup, Label, Input, FormText, Alert, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class Register extends Component {

    constructor(props) {
        super(props);
       
        this.state = {
            name: "",
            email: "",
            age: "",
            code: this.props.code.code,
            modal: false
        };

        this.toggle = this.toggle.bind(this);
        this.send = this.send.bind(this);
    }

    toggle = () => {
        this.setState(prevState => ({
          modal: !prevState.modal
        }));
      }

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    send = (props) => {
        var bodyFormData = {
            name: this.state.name,
            email: this.state.email,
            age: this.state.age,
            code: this.state.code
        }

        axios({
            method: 'post',
            url: 'http://139.59.58.237:3000/user/add',
            data: bodyFormData,
            config: { headers: { 'Content-Type': 'multipart/form-data' } }
        })
            .then(function (response) {
              
                
                alert('success');
            })
            .catch(function (response) {
                //handle error
                console.log(response);
            });
    }

    render() {
        return (
            <div className="card">
                <div style={{ display: 'inline' }}>
                    <img className="img-style" src={user} alt="Avatar" />
                    <h1>Register To System</h1>
                </div>
                {/* <audio ref="audio_tag" src={beep} controls autoPlay/> */}
                <Alert color="danger">
                    please register to system — check it out!
                </Alert>
                {/* <ReactAudioPlayer
                    src = {beep}
                    autoPlay
                /> */}
                <div style={{ textAlign: "left", paddingLeft: '15px' }}>
                    <Form>
                        <FormGroup>
                            <Label for="textName">Name</Label>
                            <Input type="text" name="name" id="name" placeholder="name" onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleEmail">Email</Label>
                            <Input type="email" name="email" id="email" placeholder="email" onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="textAge">Age</Label>
                            <Input type="text" name="name" id="age" placeholder="age" onChange={this.handleChange} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="textId">Id</Label>
                            <Input type="text" name="name" id="code" disabled={true} defaultValue={this.props.code.code} />
                        </FormGroup>
                        <Button onClick={this.send.bind(this)}>Submit</Button>
                        <Button style={{ float: "right" }} onClick={this.props.close}>Close</Button>
                    </Form>
                </div>

            </div>

        );
    }
}


export default Register;