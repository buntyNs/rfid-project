import React from 'react';
import user from '../user.svg';
import tapping from '../tapping.svg';

const Home = (props)=>{
    return(
       <div className = "card">
       <img className = "img-style-home" src={user} alt="Avatar"/>
       <h1>Welcome To The System</h1>
       <p>Please tap your student card</p>

       <img  className = "img-style" src={tapping} alt="Avatar"/>
       </div>
    
    );
}
export default Home;

